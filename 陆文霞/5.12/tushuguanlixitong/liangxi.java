package tushuguanlixitong;

import java.io.Serializable;
import java.util.Scanner;

public class liangxi {

	public static void main(String[] args) {
//采用二维数组存放用户信息（部门、用户名、密码、用户角色）
    String [][] user= new String [100][4];
//书籍信息（编码ISBN、书籍名称、价格、出版社、作者）
    String [][]book=new String[6][5];
//出版社信息（出版社名称、地址、联系人）
    String [][]pubCom =new String [100][3];
//初始化数据
//第一个用户信息，行下标为0
    user[0][0] = " HP001"; //部门
	user[0][1] = "ken";//用户名
	user[0][2] ="123";//密码
	user[0][3] ="admin";//角色
//第二个用户信息，行下表为1
	user[1][0] = " HP001"; //部门
	user[1][1] = "ken";//用户名
	user[1][2] ="123";//密码
	user[1][3] ="admin";//角色
//第三个用户信息，行下标2
	user[2][0] = "HP003";
	user[2][1] = "guile";
	user[2][2] = "123";
	user[2][3] = "user";
//书籍信息初始化
//第一个书籍信息，行下标 为0
	book[0][0] = "001";			//isbn编码
	book[0][1] = "JAVA高级编程";	//书籍名称
	book[0][2] = "100";			//价格
	book[0][3] = "铁道部出版社";	//出版社
	book[0][4] = "张三";			//作者
//第2个书籍信息：行下标 1
    book[1][0] = "002";
	book[1][1] = "SQLSERVER高级编程";
	book[1][2] = "150";
	book[1][3] = "清华出版社";
	book[1][4] = "李四";

//第3个书籍信息：行下标 2
	book[2][0] = "003";
	book[2][1] = "JAVA基础入门";
	book[2][2] = "50";
	book[2][3] = "铁道部出版社";
	book[2][4] = "张三";
//第四个信息初始化，行下标为3
	book[3][0] = "004";
	book[3][1] = "ORACLE高级编程";
	book[3][2] = "120";
	book[3][3] = "清华出版社";
	book[3][4] = "王五";
//出版社信息初始化
//第1个出版社信息：行下标 0
	pubCom[0][0] = "清华出版社";
	pubCom[0][1] = "武汉市XXXX";
	pubCom[0][2] = "aaa";

//第2个出版社信息：行下标 1
	pubCom[1][0] = "铁道出版社";
	pubCom[1][1] = "北京市XXXX";
	pubCom[1][2] = "bbb";
	//第3个出版社信息：行下标 2
	pubCom[2][0] = "邮电出版社";
	pubCom[2][1] = "南京市XXXX";
	pubCom[2][2] = "ccc";
//定义扫描器
    Scanner scanner =new Scanner(System.in);
//定义一个变量，代表是否退出系统标志，初始化true，退出系统时为false；
    boolean flag = true;
    while (flag) {
    	System.out.println("**********欢迎使用图书管理系统*********");
    	System.out.println("请输入账号:");
//用userName输入用户账号
    String userName =scanner.next();
    System.out.println("请输入密码:");
//password保存用户密码
    String password =scanner.next();
//是否找到用户相等信息
    boolean find =false;
//循环判断用户输入账号和密码是否存在用户信息中
    for (int i = 0; i < user.length; i++) {
//判断用户输入账号与user[i][1]是否相等并且密码user[i][2]是否相等
    	if (userName.equals(user [i][1])&&password.equals(user[i][2])) {
//找到用户，find标志设置为true，代表找到用户
    		find =true;
//break终止循环，已找到用户不需要继续对比判断，退出循环
    		break;
    		
		}
	}
//判断find值，如果找到用户find值为true；如果没有找到用户值乃为false；
    if (find) {
//find为true，进入系统
    	System.out.println("登录成功");
    	while (true) {
			System.out.println("1 图书管理 2出版社管理 3 退出系统");
			System.out.println("请输入数字选择功能菜单：");
//接受用户输入的数字即选择的功能
int c =scanner.nextInt();
//根据用户选择进入不同的功能
if (c==1) {
	//1图书管理
	
	
}
else if (c==2) {
	
//出版社管理
	
}
else if (c==3) {

//退出系统
//flag = false;
//break;
	System.exit(0);
	
}
else {
//选择错误提示重选
	System.out.println("选择错误，请重新选择:");
}

		}
	}
    else {
//find为false提示错误
    System.out.println("该用户不存在或者密码错误！请重新登录!");	
	}
		
	}

	}

}
