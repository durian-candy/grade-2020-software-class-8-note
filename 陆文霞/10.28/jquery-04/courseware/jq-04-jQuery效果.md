# jQuery 效果

## 本节内容

jQuery 给我们封装了很多动画效果，最为常见的如下：

- 显示隐藏： show() / hide() / toggle()
- 滑入滑出：slideDown() / slideUp() / slideToggle()
- 淡入淡出：fadeIn() / fadeOut() / fadeToggle() / fadeTo()
- 自定义动画: animate()

### 1. 显示隐藏

#### 1.1 show([speed, [easing], [fn]])

显示隐藏的匹配元素。如果这个元素是可见的，这个方法将不会改变任何东西。

> **隐藏的元素**：使用`hide()`方法隐藏的元素，css 设置了`display:none`，`visibility: hidden`的元素等。

**语法**

```
// 显示,显示隐藏的匹配元素。
show([speed, [easing], [fn]])
```

**参数**

- 参数都可以省略，没有动画直接显示。

- **speed**：三种预定速度之一的字符串（“slow”，“normal”，“fast”）或者表示动画时长的毫秒数（如：1000）。

- **easing**：用来指定切换效果，默认是“swing”，可用参数“linear”。

  > linear表示匀速直线运动，而swing则表示变速运动

- **fn**：回调函数，在动画完成时执行的函数，每个元素执行一次。

```html
<style>
  div {width: 100px; height: 100px; background-color: pink;}
</style>
<button>显示盒子</button>
<div style="display: none">Hello</div>
<script>
  $('button').click(function() {
    $('div').show();
    // 用缓慢的动画将隐藏的段落显示出来，speed参数值为slow动画历时600毫秒。
    $('div').show('slow');
    // 用快速的动画就爱过你隐藏的段落显示出来，speed参数值为fast动画历时200毫秒。并在之后执行反馈。
    $('div').show('fast', function() {
        $(this).text('动画结束！'); // this指向处理事件的元素，即p元素
    })
    // 将隐藏的段落用将近4秒的时间显示出来，并在之后执行一个反馈。
    $('div').show(4000, function() {
        $(this).text('动画结束！');
    });
  });
</script>
```

#### 1.2 hide([speed, [easing], [fn]])

隐藏显示的元素。如果选择的元素是隐藏的，则没有效果。

**语法**

```
hide([speed, [easing], [fn]])
```

**参数**

- 参数都可以省略，没有动画直接显示。
- **speed**：三种预定速度之一的字符串（“slow”，“normal”，“fast”）或者表示动画时长的毫秒数（如：1000）。
- **easing**：用来指定切换效果，默认是“swing”，可用参数“linear”。
- **fn**：回调函数，在动画完成时执行的函数，每个元素执行一次。

```html
<button>隐藏盒子</button>
<div>我是一个盒子</div>
<script>
  $('button').click(function() {
    $('div').hide();
    // 将盒子缓慢的隐藏，speed参数值为slow，历时600毫秒
    $('div').hide('slow');
    // 快速将盒子隐藏，并在之后弹框提示
    $('div').hide('fast', function() {
        alert('动画结束！');
    });
  });
</script>
```

#### 1.3 toggle([speed], [easing], [fn])

如果元素是可见的，切换为隐藏的；如果元素是隐藏的，切换为可见的。

**语法**

```
toggle([speed], [easing], [fn])
```

**参数**

- 参数都可以省略，没有动画直接显示。
- **speed**：三种预定速度之一的字符串（“slow”，“normal”，“fast”）或者表示动画时长的毫秒数（如：1000）。
- **easing**：用来指定切换效果，默认是“swing”，可用参数“linear”。
- **fn**：回调函数，在动画完成时执行的函数，每个元素执行一次。

```html
<button>显示隐藏</button>
<div>我是一个盒子</div>
<script>
  $('button').click(function() {
    $('div').toggle();
  });
</script>
```

#### 1.4 练习

设计一个页面，有一个盒子，还有几个选项，分别是显示隐藏效果函数的参数可以选取的，然后根据不同参数值，点击不同效果的按钮，可以直接看到效果。

> 分析：
>
> 1. 效果函数有3个参数，第三个参数是函数，我们不做处理。
> 2. 另外两个参数，分别有不同的值可以选择。每个可选值我们可以放在单选框中展示。
> 3. 有3个效果函数，所以我们准备3个按钮。

### 2 滑入滑出

#### 2.1 slideDown([speed], [easing], [fn])

通过**高度变化（向下增大）**来动态地显示所有匹配的元素，在显示完成后可选的触发一个回调函数。

这个动画效果只调整元素的高度，可以使匹配的元素以“滑动”的方式显示出来。

**语法**

```
slideDown([speed], [easing], [fn])
```

**参数**

- 参数都可以省略。
- **speed**：三种预定速度之一的字符串（“slow”，“normal”，“fast”）或者表示动画时长的毫秒数（如：1000）。
- **easing**：用来指定切换效果，默认是“swing”，可用参数“linear”。
- **fn**：回调函数，在动画完成时执行的函数，每个元素执行一次。

```html
<button>滑入</button>
<div style="display: none;">我是一个盒子</div>
<script>
  $('button').click(function() {
    $('div').slideDown();
    // 快速的将盒子滑下，之后弹出一个对话框
    $('div').slideDown('fast', function() {
        alert('动画结束');
    })
  });
</script>
```

#### 2.2 slideUp([speed], [easing], [fn])

通过**高度变化（向上减小）**来动态地隐藏所有匹配的元素，在隐藏完成后可选的触发一个回调函数。

这个动画效果只调整元素的高度，可以使匹配的元素以“滑动”的方式隐藏起来。

**语法**

```
slideUp([speed], [easing], [fn])
```

**参数**

- 参数都可以省略。
- **speed**：三种预定速度之一的字符串（“slow”，“normal”，“fast”）或者表示动画时长的毫秒数（如：1000）。
- **easing**：用来指定切换效果，默认是“swing”，可用参数“linear”。
- **fn**：回调函数，在动画完成时执行的函数，每个元素执行一次。

```html
<button>滑出</button>
<div>我是一个盒子</div>
<script>
  $('button').click(function() {
		$('div').slideUp();
    // 缓慢的滑上
    $('div').slideUp('slow');
    // 快速的滑上，之后弹出一个对话框
    $('div').slideUp('fast', function() {
        alert('动画结束');
    });
  });
</script>
```

#### 2.3 slideToggle([speed], [easing], [fn])

通过**高度变化**来切换所有匹配元素的可见性，并在切换完成后可选的触发一个回调函数。

这个动画效果只调整元素的高度，可以使匹配的元素以“滑动”的方式隐藏或显示。

**语法**

```
slideUp([speed], [easing], [fn])
```

**参数**

- 参数都可以省略。
- **speed**：三种预定速度之一的字符串（“slow”，“normal”，“fast”）或者表示动画时长的毫秒数（如：1000）。
- **easing**：用来指定切换效果，默认是“swing”，可用参数“linear”。
- **fn**：回调函数，在动画完成时执行的函数，每个元素执行一次。

```html
<button>滑出滑出切换</button>
<div>我是一个盒子</div>
<script>
  $('button').click(function() {
		$('div').slideToggle();
    // 缓慢的将盒子滑下或滑上，之后弹出一个对话框
    $('div').slideToggle('slow', function() {
        alert('动画结束');
    });
  })
</script>
```

### 3 淡入淡出

#### 3.1 fadeIn([speed], [easing], [fn])

通过**不透明度的变化**来实现所有匹配元素的淡入效果，并在动画完成后可选地触发一个回调函数。

这个动画只调整元素的不透明度，也就是说所有匹配的元素的高度和宽度不会发生变化。

**语法**

```
fadeIn([speed, [easing], [fn]]);
```

**参数**

- 参数都可以省略。
- **speed**：三种预定速度之一的字符串（“slow”，“normal”，“fast”）或者表示动画时长的毫秒数（如：1000）。
- **easing**：用来指定切换效果，默认是“swing”，可用参数“linear”。
- **fn**：回调函数，在动画完成时执行的函数，每个元素执行一次。

```html
<button>淡入</button>
<div>我是一个盒子</div>
<script>
  $('button').click(function() {
		$('div').fadeIn();
  })
</script>
```

#### 3.2 fadeOut([speed], [easing], [fn])

通过**不透明度的变化**来实现所有匹配元素的淡出效果，并在动画完成后可选地触发一个回调函数。

这个动画只调整元素的不透明度，也就是说所有匹配的元素的高度和宽度不会发生变化。

**语法**

```
fadeOut([speed, [easing], [fn]]);
```

**参数**

- 参数都可以省略。
- **speed**：三种预定速度之一的字符串（“slow”，“normal”，“fast”）或者表示动画时长的毫秒数（如：1000）。
- **easing**：用来指定切换效果，默认是“swing”，可用参数“linear”。
- **fn**：回调函数，在动画完成时执行的函数，每个元素执行一次。

```html
<button>淡出</button>
<div>我是一个盒子</div>
<script>
  $('button').click(function() {
		$('div').fadeOut();
  })
</script>
```

#### 3.3 fadeTo([[speed], opacity, [easing], [fn]])

把所有匹配元素的不透明度以渐进方式调整到指定的不透明度，并在动画完成后可选地触发一个回调函数。

这个动画只调整元素的不透明度，也就是说所有匹配的元素的高度和宽度不会发生变化。

**语法**

```
fadeTo([speed, opacity, [easing], [fn]]);
```

**参数**

- 参数都可以省略。
- **opacity**：一个0~1之间表示透明度的数字。
- **speed**：三种预定速度之一的字符串（“slow”，“normal”，“fast”）或者表示动画时长的毫秒数（如：1000）。
- **easing**：用来指定切换效果，默认是“swing”，可用参数“linear”。
- **fn**：回调函数，在动画完成时执行的函数，每个元素执行一次。

```html
<div>我是一个盒子</div>
<button id="btn">fadeTo</button>
<script>
    $('#btn').click(function() {
        // 将一个元素的透明度调整到0.66
        $('div').fadeTo('slow', 0.66);
    })
</script>
```

#### 3.4 fadeToggle([speed], [easing], [fn])

通过**不透明度的变化**来开关所有匹配元素的淡入和淡出效果，并在动画完成后可选地触发一个回调函数。

这个动画只调整元素的不透明度，也就是说所有匹配的元素的高度和宽度不会发生变化。

**语法**

```
fadeToggle([speed, [easing], [fn]]);
```

**参数**

- 参数都可以省略。
- **speed**：三种预定速度之一的字符串（“slow”，“normal”，“fast”）或者表示动画时长的毫秒数（如：1000）。
- **easing**：用来指定切换效果，默认是“swing”，可用参数“linear”。
- **fn**：回调函数，在动画完成时执行的函数，每个元素执行一次。

```html
<div>我是一个盒子</div>
<button id="btn">fadeToggle</button>
<script>
    $('#btn').click(function() {
        // 将一个元素的透明度调整到0.66
        $('div').fadeToggle('slow', 'linear', function() {
            alert('动画结束');
        });
    })
</script>
```

## 作业

1. 参考显示隐藏的练习案例，给滑入滑出的动画事件，制作一样的页面。

2. 参考显示隐藏的练习案例，给淡入淡出的动画事件，制作一样的页面。注意，fadeTo方法的opacity的值，请用滑块元素来实现。

   > 滑块元素：` <input type="range">`

