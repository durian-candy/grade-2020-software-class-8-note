$(function() {
    let categories = [
        {name: '西部数据（WD）', img: '03-wd.jpg', initial: 'X'},
        {name: '东芝（TOSHIBA）', img: '03-toshiba.png', initial: 'D'},
        {name: '希捷（SEAGATE）', img: '03-seagate.jpg', initial: 'X'},
        {name: '海康威视（HIKVISION）', img: '03-hikvision.jpg', initial: 'H'},
        {name: '联想（lenovo）', img: '03-lenovo.png', initial: 'L'},
        {name: '万人迷（manovo）', img: '', initial: 'M'},
        {name: '三星（SAMSUNG）', img: '03-samsung.jpg', initial: 'S'},
        {name: '七彩虹（Colorful）', img: '03-colorful.png', initial: 'Q'},
        {name: '布谷鸟', img: '', initial: 'B'},
        {name: '金士顿（Kingston）', img: '03-kingston.jpg', initial: 'J'},
        {name: '京天（KOTIN）', img: '03-kotin.jpg', initial: 'J'},
        {name: '华硕（ASUS）', img: '03-asus.jpg', initial: 'H'},
        {name: 'CC&JBL', img: '03-cc&jbl.jpg', initial: 'C'},
        {name: 'KU&SI', img: '03-kusi.jpg', initial: 'K'},
        {name: '灵蛇', img: '03-lingshe.png', initial: 'L'},
        {name: '铠侠（Kioxia）', img: '03-kioxia.jpg', initial: 'K'},
        {name: '影驰', img: '03-yingchi.png', initial: 'Y'},
        {name: 'dahua', img: '03-dahua.jpg', initial: 'D'},
        {name: '瀚达彩', img: '03-hdc.jpg', initial: 'H'}
    ];
    $('.filter-char span').hover(function(event) {
        console.log('aa')
        let initial = this.dataset.initial;
        console.log(initial);
        if (initial === '0') {
            $('.show-box li').show();
        } else {
            $('.show-box li').hide();
            console.log($('.show-box li'));
            $(`.show-box li[data-initial=${initial}]`).show()
        }
    })
    $('.action-panel > div').onclick(function(event){
        
    })
})