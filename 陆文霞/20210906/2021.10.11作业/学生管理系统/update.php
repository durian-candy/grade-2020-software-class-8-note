<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/6/28
 * Time: 15:29
 */
$studentId=$_GET['studentId'];
$dsn = "sqlsrv:Server=localhost;Database=Student";
$db = new PDO($dsn, "sa", "123456");
$sql="select *from Student where StudentId='{$studentId}'";
$result = $db->query($sql);
$studentInfo=($result->fetch(PDO::FETCH_ASSOC));
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>修改</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
</head>
<body>
<div id="container">
    <form method="post" action="update_save.php">
        <table class="update">
            <caption>
                <h3>修改学生信息</h3>
            </caption>
            <tr>
                <td>学生id：</td>
                <td><input type="text" name="studentId" value="<?php echo $studentInfo['StudentId']?>" readonly="readonly"/></td>
            </tr>
            <tr>
                <td>学生姓名：</td>
                <td><input type="text" name="studentName" value="<?php echo $studentInfo['StudentName']?>"/></td>
            </tr>
            <tr>
                <td>学生性别：</td>
                <td>
                    <input type="radio" name="studentSex" <?php
                    if ($studentInfo['StudentSex']==1){
                        echo 'checked="checked"';
                    }
                    ?>value="1" />男
                    <input type="radio" name="studentSex" <?php
                    if ($studentInfo['StudentSex']==2){
                        echo 'checked="checked"';
                    }
                    ?>value="2" />女

                </td>
            </tr>
            <tr>
                <td>学生生日：</td>
                <td><input type="text"name="studentBirth" value="<?php echo  $studentInfo['StudentBirth']?>"/></td>
            </tr>
            <tr>
                <td>学生住址：</td>
                <td><input type="text" name="studentAddress" value="<?php echo $studentInfo['StudentAddress']?>"/></td>
            </tr>
            <tr>
                <td>学生状态：</td>
                <td>
                    <input type="radio" name="studentStatus" <?php
                    if ($studentInfo['StudentStatus']==1){
                        echo 'checked="checked"';
                    }
                    ?>value="1" />在校
                    <input type="radio" name="studentStatus" <?php
                    if ($studentInfo['StudentStatus']==2){
                        echo 'checked="checked"';
                    }
                    ?>value="2" />毕业

                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" value="提交" class="btn" />
                    <input type="reset" value="重置" class="btn" />
                </td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>

