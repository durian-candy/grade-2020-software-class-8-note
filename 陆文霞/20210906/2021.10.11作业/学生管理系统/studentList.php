<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/6/28
 * Time: 14:54
 */
//1. 按照学生id降序排列。
//2. 性别需要显示：男或女。
//3. 状态需要显示：在校或毕业。
//4. 只有状态为 在校 的学生，才显示 标记为毕业 操作链接。
//连接数据库
$dsn = "sqlsrv:Server=localhost;Database=Student";
$db = new PDO($dsn, "sa", "123456");
$sql="select *from Student order by StudentId desc";
$result = $db->query($sql);
$classList=($result->fetchAll(PDO::FETCH_ASSOC));
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>列表</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
</head>
<body>
<div id="container">
    <a id="add" href="add.php">增加</a>
    <table class="list">
        <tr>
            <th>学生id</th>
            <th>学生姓名</th>
            <th>性别</th>
            <th>生日</th>
            <th>家庭住址</th>
            <th>状态</th>
            <th>操作</th>
        </tr>
        <?php  foreach ($classList as $key=>$value):?>
        <tr>
            <td><?php echo $value['StudentId'] ?></td>
            <td><?php echo $value['StudentName'] ?></td>
            <td><?php if ($value['StudentSex']==1){
                echo '男';
                } elseif ($value['StudentSex']==2){
                echo '女';
                }
                ?></td>
            <td><?php echo $value['StudentBirth'] ?></td>
            <td><?php echo $value['StudentAddress'] ?></td>
            <td><?php if ($value['StudentStatus']==1){
                echo '在校';
                }else if ($value['StudentStatus']==2){
                echo '毕业';
                } ?></td>
            <td>
                <a href="update.php?studentId=<?php echo  $value['StudentId']?>">修改</a>
                <a href="ji.php?studentId=<?php  echo  $value['StudentId']?>"><?php
                    if ($value['StudentStatus']==1){
                        echo '标记为毕业';
                    }else if ($value['StudentStatus']==2){
                        echo '';
                    }

                    ?></a>
                <a href="delete.php?studentId=<?php echo  $value['StudentId']?>">删除</a>
            </td>
        </tr>
        <?php endforeach;?>
    </table>
</div>
</body>
</html>

