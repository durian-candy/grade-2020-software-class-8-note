<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/6/28
 * Time: 15:10
 */
//1. 性别默认选中：男。
//2. 点击提交，则保存成功。
//3. 学生状态StudentStatus字段，默认值为： 1，表示在校。
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>增加</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
</head>
<body>
<div id="container">
    <form method="post" action="add_save.php">
        <table class="update">
            <caption>
                <h3>增加学生信息</h3>
            </caption>
            <tr>
                <td>学生姓名：</td>
                <td><input type="text" name="studentName"/></td>
            </tr>
            <tr>
                <td>学生性别：</td>
                <td>
                   <input type="radio" name="studentSex" checked="checked" value="1">男
                    <input type="radio" name="studentSex"  value="2">女
                </td>
            </tr>
            <tr>
                <td>学生生日：</td>
                <td><input type="text" name="studentBirth"/></td>
            </tr>
            <tr>
                <td>学生住址：</td>
                <td><input type="text" name="studentAddress"/></td>
            </tr>
            <tr>
                <td>学生状态：</td>
                <td>
                   <input type="radio" name="studentStatus" checked="checked" value="1">在校
                    <input type="radio" name="studentStatus"  value="2">毕业
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" value="提交" class="btn" />
                    <input type="reset" value="重置" class="btn" />
                </td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>

