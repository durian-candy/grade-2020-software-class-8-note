    create table Class(
	ClassId int not null identity(1,1),
	ClassName nvarchar(50) not null
     );
 go

	create table Student (
	StudentId int not null identity(1, 1),
	StudentName nvarchar(50),
	StudentSex tinyint not null,
	StudentBirth date,
	StudentAddress nvarchar(255) not null
);
go

    create table Course(
	CourseId int identity(1,1),
	CourseName nvarchar(50),
	CourseCredit int
);
go
    create table ClassCourse(
	ClassCourseId int identity(1,1),
	ClassId int,
	CourseId int
);
go

    create table Score(
	ScoreId int identity(1,1),
	StudentId int,
	CourseId int,
	Score int
);
go
     alter table Student add ClassId int not null;
go
     alter table Class add constraint PK_Class_ClassId primary key (ClassId);
     -- 学生表 StudentId 字段需要设置为主键（主键约束）
     alter table Student add constraint PK_Student_StudentId primary key (StudentId);
     -- 课程表 CourseId 字段需要设置为主键（主键约束）
     alter table Course add constraint PK_Course_CourseId primary key (CourseId);
     -- 班级课程表 ClassCourseId 字段需要设置为主键（主键约束）
     alter table ClassCourse add constraint PK_ClassCourse_ClassCourseId primary key (ClassCourseId);
     -- 分数表 ScoreId 字段需要设置为主键（主键约束）
     alter table Score add constraint PK_Score_ScoreId primary key (ScoreId);

     -- 学生表 StudentName 不允许为空（非空约束）
     alter table Student alter column StudentName nvarchar(50) not null;

     -- 班级表 ClassName 需要唯一（唯一约束）
     alter table Class add constraint UQ_Class_ClassName unique(ClassName);
     -- 课程表 CourseName 需要唯一（唯一约束）
     alter table Course add constraint UQ_Course_CourseName unique(CourseName);

     -- 学生表 ClassId 增加默认值为0（默认值约束）
     alter table Student add constraint DF_Student_ClassId default(0) for ClassId;

      -- 学生表 StudentSex 只能为1或者2（Check约束）
      alter table Student add constraint CK_Student_StudentSex check(StudentSex=1 or StudentSex=2 or StudentSex=3);
      -- 分数表 Score 字段只能大于等于0（check约束）
      alter table Score add constraint CK_Score_Score check(Score>=0);
      -- 课程表 CourseCredit 字段只能大于0（check约束）
      alter table Course add constraint CK_Course_CourseCredit check(CourseCredit>=0);

      -- 班级课程表ClassId 对应是 班级表ClassId 的外键 （外键约束）
      alter table ClassCourse add constraint FK_ClassCourse_ClassId foreign key (ClassId) references Class(ClassId);
      -- 班级课程表CourseId 对应是 课程表CourseId 的外键 （外键约束）
      alter table ClassCourse add constraint FK_ClassCourse_CourseId foreign key (CourseId) references Course(CourseId);
      -- 分数表StudentId 对应是 学生表StudentId 的外键 （外键约束）
      alter table Score add constraint FK_Score_StudentId foreign key (StudentId) references Student(StudentId);
      -- 分数表CourseId 对应是 课程表CourseId 的外键 （外键约束）
      alter table Score add constraint FK_Score_CourseId foreign key (CourseId) references Course(CourseId);

        select *  from   Class 
        select *  from   Student
        select *  from      Course
        select *  from   ClassCourse
        select *  from   Score


      --1. 学校开设了3个班级：软件一班、软件二班、计算机应用技术班。请插入班级表相关数据。
      insert into  Class (ClassName) values ('软件一班');

      insert into  Class (ClassName) values ('软件二班');

      insert into  Class (ClassName) values ('计算机应用技术班');

	  -- 2. 软件一班有3个同学，姓名、性别、生日、家庭住址  分别是：
	
	  --- 刘正、男、2000-01-01、广西省桂林市七星区空明西路10号鸾东小区
	  --- 黄贵、男、2001-03-20、江西省南昌市青山湖区艾溪湖南路南150米广阳小区
	  --- 陈美、女、2000-07-08、福建省龙岩市新罗区曹溪街道万达小区
	  
	  insert into Student(
	  StudentName ,
	  StudentSex ,
	  StudentBirth ,
	  StudentAddress,
	  ClassId)values(' 刘正','1','2000-01-01','广西省桂林市七星区空明西路10号鸾东小区','1');

	  insert into Student(
	  StudentName ,
	  StudentSex ,
	  StudentBirth ,
	  StudentAddress, ClassId)values('黄贵','1','2001-03-20','江西省南昌市青山湖区艾溪湖南路南150米广阳小区','1');

	  insert into Student(
	  StudentName ,
	  StudentSex ,
	  StudentBirth ,
	  StudentAddress, ClassId)values('陈美','2','2000-07-08','福建省龙岩市新罗区曹溪街道万达小区 ','1');


	  select *  from   Class 
      select *  from   Student
      select *  from      Course
      select *  from   ClassCourse
      select *  from   Score

      -- 3. 软件二班有2个同学，姓名、性别、生日、家庭住址  分别是：

	  --- 江文、男、2000-08-10、安徽省合肥市庐阳区四里河路与潜山路交汇处万科城市之光
	  --- 钟琪、女、2001-03-21、湖南省长沙市雨花区红花坡社区

	  insert into Student(  StudentName , StudentSex ,
	  StudentBirth ,
	  StudentAddress,ClassId)values('江文','1','2000-08-10','安徽省合肥市庐阳区四里河路与潜山路交汇处万科城市之光','2');

      insert into Student(  StudentName ,
	  StudentSex ,
	  StudentBirth ,
	  StudentAddress,ClassId)values(' 钟琪','2','2001-03-21','湖南省长沙市雨花区红花坡社区','2');
	
	  -- 4. 计算机应用技术班有4个同学，姓名、性别、生日、家庭住址  分别是：
	  --- 曾小林、男、1999-12-10、安徽省合肥市庐阳区四里河路与潜山路交汇处万科城市之光
	  --- 欧阳天天、女、2000-04-05、湖北省武汉市洪山区友谊大道与二环线交汇处融侨悦府
	  --- 徐长卿、男、2001-01-30、江苏省苏州市苏州工业园区独墅湖社区
	  --- 李逍遥、男、1999-11-11、广东省广州市白云区金沙洲岛御洲三街恒大绿洲

	  insert into Student(  StudentName , StudentSex , StudentBirth ,
	  StudentAddress,ClassId)
	  values(' 曾小林','1','1999-12-10','安徽省合肥市庐阳区四里河路与潜山路交汇处万科城市之光','3'),
	        (' 欧阳天天','2','2000-04-05','湖北省武汉市洪山区友谊大道与二环线交汇处融侨悦府','3'),
			(' 徐长卿','1','2001-01-30','江苏省苏州市苏州工业园区独墅湖社区','3'),
			('李逍遥','1','1999-11-1','广东省广州市白云区金沙洲岛御洲三街恒大绿洲','3')
			
	  
	  select *  from   Class
      select *  from   Student
      select *  from      Course
      select *  from   ClassCourse
      select *  from   Score

	  -- 5. 有2个学生尚未分配班级，姓名、性别、生日、家庭住址  分别是：

	  --- 东方不败 保密 1999-12-11 河北省平定州西北四十余里的猩猩滩
	  --- 令狐冲 男 2000-08-11 陕西省渭南市华阴市玉泉路南段

	  insert into Student(  StudentName ,
	  StudentSex ,
	  StudentBirth ,
	  StudentAddress)
	  values('东方不败','3','1999-12-11','河北省平定州西北四十余里的猩猩滩'),
	        ('令狐冲','1','2000-08-11','陕西省渭南市华阴市玉泉路南段')
	        



